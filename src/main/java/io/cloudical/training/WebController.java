package io.cloudical.training;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebController {

	@Value("${instance.name}")
	private String instanceName;
	
	@GetMapping("/")
	public String getValues(Model model) {
		// Add the given value to the model
		model.addAttribute("instanceName", instanceName);
		
		// Display the index view
		return "index";
	}
	
}
